const jwt = require('jsonwebtoken')
const User = require('./models/user')

const getUser = async (token) => {
    try {
        // const token = token.replace('Bearer ', '')
        const decoded = jwt.verify(token, process.env.JWT_SECRET)
        const user = await User.findOne({ _id: decoded._id, 'tokens.token': token })

        return user;
        
    } catch (e) {
        return e;
    }
}

module.exports = {
    getUser
}