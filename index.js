const { ApolloServer } = require("apollo-server");
const { typeDefs } = require("./schema/type-defs");
const { resolvers } = require("./schema/resolvers");
const { getUser } = require("./utils")
require('./schema/mongoose')

const server = new ApolloServer({ 
    typeDefs, 
    resolvers,
    context: async ({ req }) => {
        if (req && req.headers.authorization) {
            user = await getUser(req.headers.authorization);
        } else {
            user = null;
        }
        return {
            user
        }
    }
});

server.listen().then(( {url} ) => {
    console.log("Server is ready at " + url);
})