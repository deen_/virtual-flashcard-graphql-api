const { gql } = require("apollo-server");

exports.typeDefs = gql`
    type Query {
        users: [User!]
        user(id: ID!): User
        decks(filter: DecksFilterInput): [Deck!]!
        deck(id: ID!): Deck
        cards(deck_id: ID!): [Card!]
        card(id: ID!): Card
    }

    type Mutation {
        login(input: LoginInput): AuthPayload!
        logout: Boolean
        register(input: RegisterInput): User!
        addDeck(input: AddDeckInput): Deck!
        addCard(input: AddCardInput): Card!
        updateUser(id: ID!, input: UpdateUserInput): User!
        updateDeck(id: ID!, input: UpdateDeckInput): Deck!
        updateCard(id: ID!, input: UpdateCardInput): Card!
    }

    type User {
        id: ID!
        name: String
        email: String!
        decks: [Deck!]
    }

    type Deck {
        id: ID!
        description: String!
        public: Boolean!
        cards: [Card!]
        owner: User!
    }

    type Card {
        id: ID!
        front: String!
        back: String!
        deck: Deck
        parent_deck: String!
    }

    type AuthPayload {
        token: String
        user: User
    }

    input DecksFilterInput {
        public: Boolean
    }

    input LoginInput {
        email: String!
        password: String!
    }

    input RegisterInput {
        name: String!
        email: String!
        password: String
    }

    input AddDeckInput {
        description: String!
        public: Boolean!
        owner: ID!
    }

    input AddCardInput {
        front: String!
        back: String!
        parent_deck: ID!
    }

    input UpdateUserInput {
        name: String
        password: String
    }

    input UpdateDeckInput {
        description: String
        public: Boolean
        owner: ID
    }

    input UpdateCardInput {
        front: String
        back: String
        parent_deck: ID
    }
`