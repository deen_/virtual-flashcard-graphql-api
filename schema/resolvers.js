const { Query } = require("../resolvers/Query");
const { Mutation } = require("../resolvers/Mutation");
const { User } = require("../resolvers/User");
const { Deck } = require("../resolvers/Deck");
const { Card } = require("../resolvers/Card");

const resolvers = {
    Query,
    Mutation,
    User,
    Deck,
    Card
}

module.exports = { resolvers };