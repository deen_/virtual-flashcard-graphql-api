const User = require('../models/user')
const Deck = require('../models/deck')
const Card = require('../models/card')

exports.Mutation = {
    login: async (_, { input }) => {
        const user = await User.findByCredentials(input.email, input.password)
        
        const token = await user.generateAuthToken();

        return {
            token,
            user
        };
    },

    logout: async (_, args, { user }) => {
        try {
            user.tokens = user.tokens.filter((token) => {
                return token.token !== token
            })
            await user.save()
    
            return true;
        } catch (e) {
            return false;
        }
    },

    register: async (_, { input }) => {
        const user = new User(input);

        try {
            await user.save();
            return user;
        } catch(e) {
            return e;
        }
    },

    addDeck: async (_, { input }, { user }) => {
        if (user) {
            const deck = new Deck(input);

            try {
                deck.owner = user.id;
                await deck.save();
                return deck;
            } catch(e) {
                return e;
            }    
        }
        return null;
    },

    addCard: async (_, { input }, { user }) => {
        const card = new Card(input);

        try {
            await card.save();
            return card;
        } catch(e) {
            return e;
        }
    },

    updateUser: async (_, { id, input }, { user }) => {
        if (id == user.id) {
            const updates = Object.keys(input);
            const allowedUpdates = ["name", "password"];
            const isValidOperation = updates.every((update) => allowedUpdates.includes(update));

            if (!isValidOperation) {
                return "Error";
            }

            try {
                updates.forEach((update) => user[update] = input[update])
                await user.save();
                return user;
            } catch (e) {
                return e;
            }    
        }
        return null;
    },

    updateDeck: async (_, { id, input }, { user }) => {
        const deck = await Deck.findById(id);

        if (deck.owner == user.id) {
            const updates = Object.keys(input);
            const allowedUpdates = ["description", "public"];
            const isValidOperation = updates.every((update) => allowedUpdates.includes(update));
    
            if (!isValidOperation) {
                return "Error";
            }
    
            try {
                updates.forEach((update) => deck[update] = input[update])
                await deck.save();
                return deck;
            } catch (e) {
                return e;
            }
        } 
        return null;
    },

    updateCard: async (_, { id, input }, { user }) => {
        const card = await Card.findById(id);
        const deck = await Card.findById(card.parent_deck);

        if (deck.owner == user.id) {
            const updates = Object.keys(input);
            const allowedUpdates = ["front", "back"];
            const isValidOperation = updates.every((update) => allowedUpdates.includes(update));

            if (!isValidOperation) {
                return "Error";
            }

            try {
                updates.forEach((update) => card[update] = input[update])
                await card.save();
                return card;
            } catch (e) {
                return e;
            }    
        }
        return null;        
    }
};