const User = require('../models/user')
const Deck = require('../models/deck')
const Card = require('../models/card')

exports.Query = {
    users: async(_, args, {user}) => {
        if (user) {
            return await User.find();
        } 
        return null;
    },
    user: async(_, {id}, {user}) => {
        if (user.id == id) {
            return await User.findById(id);
        } 
        return null;
    },
    decks: async(_, {filter}, {user}) => {
        if (user.id) {
            if (filter && filter.public) {
                const { public } = filter;
                return await Deck.find({ $or: [ {public: true}, {owner: user.id} ] });
            }
            return await Deck.find({ owner: user.id });
        }
        return await Deck.find({ public: true });
    },
    deck: async(_, {id}, {user}) => {
        const deck = await Deck.findById(id);

        if (user.id == deck.owner) {
            return deck;
        }
        return null;
    },
    cards: async(_, {deck_id}, {user}) => {
        const deck = await Deck.findById(deck_id);

        console.log(deck)

        if (user.id == deck.owner) {
            return await Card.find({ parent_deck: deck_id });
        }
        return null;
    },
    card: async(_, {id}, {user}) => {
        const card = await Card.findById(id);
        const deck = await Deck.findOne({ _id: card.parent_deck });

        if (user.id == deck.owner) {
            return card;
        }
        return null;
    }
};