const Deck = require('../models/deck')

exports.User = {
    decks: async({ id }) => {
        return await Deck.find({ owner: id })
    }
};