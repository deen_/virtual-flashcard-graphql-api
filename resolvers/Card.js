const Deck = require('../models/deck')

exports.Card = {
    deck: async({ parent_deck }) => {
        return await Deck.findOne({ _id: parent_deck })
    }
};