const User = require('../models/user')
const Card = require('../models/card')

exports.Deck = {
    owner: async({ owner }) => {
        return await User.findOne({ _id: owner })
    },
    cards: async({ id }) => {
        return await Card.find({ parent_deck: id })
    }
};